# aschild1

> Test peer deps child 1

[![NPM](https://img.shields.io/npm/v/aschild1.svg)](https://www.npmjs.com/package/aschild1) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save aschild1
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'aschild1'
import 'aschild1/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [](https://github.com/)
