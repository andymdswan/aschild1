import React from 'react';
import { ExampleComponent } from 'aschild2';

var styles = {"test":"_3ybTi"};

var ExampleComponentChild1 = function ExampleComponentChild1(_ref) {
  var showChild2 = _ref.showChild2;
  return typeof showChild2 !== 'undefined' && showChild2 ? /*#__PURE__*/React.createElement(ExampleComponent, null) : /*#__PURE__*/React.createElement("div", {
    className: styles.test
  }, "Example Component: Child 1");
};

export { ExampleComponentChild1 };
//# sourceMappingURL=index.modern.js.map
