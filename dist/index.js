function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var React = _interopDefault(require('react'));
var aschild2 = require('aschild2');

var styles = {"test":"_3ybTi"};

var ExampleComponentChild1 = function ExampleComponentChild1(_ref) {
  var showChild2 = _ref.showChild2;
  return typeof showChild2 !== 'undefined' && showChild2 ? /*#__PURE__*/React.createElement(aschild2.ExampleComponent, null) : /*#__PURE__*/React.createElement("div", {
    className: styles.test
  }, "Example Component: Child 1");
};

exports.ExampleComponentChild1 = ExampleComponentChild1;
//# sourceMappingURL=index.js.map
