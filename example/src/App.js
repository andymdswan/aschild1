import React from 'react'

import { ExampleComponentChild1 } from 'aschild1'
import 'aschild1/dist/index.css'

const App = () => {
  return <ExampleComponentChild1 showChild2={true} />
}

export default App
