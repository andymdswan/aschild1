import React from 'react'
import styles from './styles.module.css'
import { ExampleComponent } from 'aschild2'

export const ExampleComponentChild1 = ({ showChild2 }) => {
  return typeof showChild2 !== 'undefined' && showChild2 ? (
    <ExampleComponent />
  ) : (
    <div className={styles.test}>Example Component: Child 1</div>
  )
}
